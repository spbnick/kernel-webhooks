"""Clone or update the kernel-watch repo."""
import argparse
import os
import subprocess
import sys
import time
from urllib import parse

from cki_lib import logger
from cki_lib import misc
import sentry_sdk

from .. import common

LOGGER = logger.get_logger('cki.webhook.utils.update_kernel_watch')


def main(args):
    """Run main loop."""
    parser = argparse.ArgumentParser(description='Clone or update the kernel-watch repo')
    parser.add_argument('--sentry-ca-certs', default=os.getenv('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    parser.add_argument('--update-interval-minutes', type=int,
                        help='Continuously update with the given interval')
    parser.add_argument('--local-repo-path', **common.get_argparse_environ_opts('LOCAL_REPO_PATH'),
                        help='Local path where the kernel-watch repo is checked out')
    parser.add_argument('--kernel-watch-url',
                        **common.get_argparse_environ_opts('KERNEL_WATCH_URL'),
                        help='URL of the kernel-watch project')
    args = parser.parse_args(args)

    misc.sentry_init(sentry_sdk, ca_certs=args.sentry_ca_certs)

    os.makedirs(args.local_repo_path, exist_ok=True)
    while True:
        if os.path.isfile(f'{args.local_repo_path}/.git/config'):
            LOGGER.info('Updating kernel-watch repo')
            with misc.only_log_exceptions():
                subprocess.run(['git', 'pull'], cwd=args.local_repo_path, check=True)
        else:
            LOGGER.info('Cloning kernel-watch repo')
            url_parts = parse.urlparse(args.kernel_watch_url)
            git_url = parse.urlunparse(url_parts._replace(
                netloc=f'oauth2:{os.environ["COM_GITLAB_RO_REPO_TOKEN"]}@{url_parts.netloc}'))
            subprocess.run(['git', 'clone', git_url, args.local_repo_path], check=True)

        if not args.update_interval_minutes:
            break

        LOGGER.info('Sleeping for %s minutes', args.update_interval_minutes)
        time.sleep(60 * args.update_interval_minutes)


if __name__ == "__main__":
    main(sys.argv[1:])
