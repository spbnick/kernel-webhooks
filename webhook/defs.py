"""Common variable definitions that can be used by all webhooks (and common code)."""
from enum import IntEnum
from enum import auto

EMAIL_BRIDGE_ACCOUNT = 'redhat-patchlab'
BOT_ACCOUNTS = ('cki-bot', 'cki-kwf-bot', EMAIL_BRIDGE_ACCOUNT)
JIRA_BOT_ACCOUNTS = ('gitlab-jira', 'gitlab-bot')
ARK_PROJECT_ID = 13604247

MAX_COMMITS_PER_MR = 2000
MAX_COMMITS_PER_COMMENT_ROW = 20
TABLE_ENTRY_THRESHOLD = 5

UMB_BRIDGE_MESSAGE_TYPE = 'cki.kwf.umb-bz-event'
JIRA_WEBHOOK_MESSAGE_TYPE = 'jira'
JPFX = 'RHEL-'
JIRA_SERVER = 'https://issues.redhat.com/'

LABELS_YAML_PATH = 'utils/labels.yaml'
RH_METADATA_YAML_PATH = 'utils/rh_metadata.yaml'

GITFORGE = 'https://gitlab.com'
CENTOS_STREAM_KERNEL_NAMESPACE = 'redhat/centos-stream/src/kernel'
RHEL_KERNEL_NAMESPACE = 'redhat/rhel/src/kernel'

CONFIG_LABEL = 'Configuration'
NEEDS_REVIEW_SUFFIX = 'NeedsReview'
NEEDS_TESTING_SUFFIX = 'NeedsTesting'
MISSING_SUFFIX = 'Missing'
TESTING_FAILED_SUFFIX = 'TestingFailed'
TESTING_WAIVED_SUFFIX = 'Waived'
READY_SUFFIX = 'OK'
BLOCKED_BY_PREFIX = 'Blocked-by:'
BLOCKED_SUFFIX = 'Blocked'
TESTING_SUFFIXES = (NEEDS_TESTING_SUFFIX, TESTING_FAILED_SUFFIX)

BASE_DEPENDENCIES = [f'Acks::{READY_SUFFIX}',
                     f'CKI::{READY_SUFFIX}',
                     f'CommitRefs::{READY_SUFFIX}',
                     f'Merge::{READY_SUFFIX}',
                     f'Signoff::{READY_SUFFIX}']

# We are removing Bugzilla from the deps and not adding Jira explicitly, until
# such time as we don't have to deal with both. The overlap is being handled
# by common.get_required_ticket_labels.
# READY_FOR_MERGE_DEPS = BASE_DEPENDENCIES + [f'Bugzilla::{READY_SUFFIX}',
#                                            f'Dependencies::{READY_SUFFIX}']
# READY_FOR_QA_DEPS = BASE_DEPENDENCIES + [f'Bugzilla::{NEEDS_TESTING_SUFFIX}']
READY_FOR_MERGE_DEPS = BASE_DEPENDENCIES + [f'Dependencies::{READY_SUFFIX}']
READY_FOR_QA_DEPS = BASE_DEPENDENCIES

ARK_READY_FOR_MERGE_DEPS = [f'Acks::{READY_SUFFIX}', f'CKI::{READY_SUFFIX}']

READY_FOR_MERGE_LABEL = 'readyForMerge'
READY_FOR_QA_LABEL = 'readyForQA'
MERGE_CONFLICT_LABEL = 'Merge::Conflicts'
MERGE_WARNING_LABEL = 'Merge::Warning'
TARGETED_TESTING_LABEL = 'TargetedTestingMissing'

BUG_FIELDS = ['cf_internal_target_release',
              'cf_verified',
              'cf_zstream_target_release',
              'component',
              'external_bugs',
              'flags',
              'id',
              'product',
              'status',
              'summary'
              ]

EXT_TYPE_URL = 'https://gitlab.com/'
CODE_CHANGED_PREFIX = "CodeChanged::"

DCO_URL = "https://developercertificate.org"
DCO_PASS = "The DCO Signoff Check for all commits and the MR description has **PASSED**.\n"
DCO_FAIL = ("**ERROR: DCO 'Signed-off-by:' tags were not found on all commits and the MR "
            "description. Please review the results in the table below.**  \n"
            "This project requires developers add a Merge Request description and per-commit "
            f"acknowlegement of the [Developer Certificate of Origin]({DCO_URL}), also known "
            "as the DCO. This can be accomplished by adding an explicit 'Signed-off-by:' tag "
            "to your MR description and each commit.\n\n"
            "**This Merge Request's commits will not be considered for inclusion into this "
            "project until these problems are resolved. After making the required changes please "
            "resubmit your merge request for review.**\n\n")
SUBSYS_LABEL_PREFIX = 'Subsystem'
NOTIFICATION_HEADER = "Notifying users:"
NOTIFICATION_TEMPLATE = ("{header} {users}  \nThis is the Subsystems hook's user notification"
                         " system for file changes. Please see the"
                         " [kernel-watch project]({project}) for details.")

INTERNAL_FILES = ('redhat/', '.gitlab', '.get_maintainer.conf', 'makefile', 'Makefile.rhelver')


class BZPriority(IntEnum):
    """Possible priority of a bugzilla BZ."""

    # https://bugzilla.redhat.com/page.cgi?id=fields.html#priority
    UNKNOWN = auto()
    UNSPECIFIED = auto()
    LOW = auto()
    MEDIUM = auto()
    HIGH = auto()
    URGENT = auto()

    @classmethod
    def from_str(cls, priority_str):
        """Return the BZPriority matching the string, or BZPriority.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == priority_str.upper()), cls.UNKNOWN)


class JIPriority(IntEnum):
    """Possible priority of a JIRA Issue."""

    UNKNOWN = auto()
    UNSPECIFIED = auto()
    LOW = auto()
    MEDIUM = auto()
    HIGH = auto()
    URGENT = auto()

    @classmethod
    def from_str(cls, priority_str):
        """Return the JIPriority matching the string, or JIPriority.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == priority_str.upper()), cls.UNKNOWN)


class BZStatus(IntEnum):
    """Possible status of a bugzilla BZ."""

    # https://bugzilla.redhat.com/page.cgi?id=fields.html#bug_status
    UNKNOWN = auto()
    NEW = auto()
    ASSIGNED = auto()
    POST = auto()
    MODIFIED = auto()
    ON_DEV = auto()
    ON_QA = auto()
    VERIFIED = auto()
    RELEASE_PENDING = auto()
    CLOSED = auto()

    @classmethod
    def from_str(cls, status):
        """Return the BZStatus matching the status string, or BZStatus.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == status.upper()), cls.UNKNOWN)


class JIStatus(IntEnum):
    """Possible status of a JIRA Issue."""

    # Red Hat's JIRA project has New (id=11), Planning (id=81), Development (id=91),
    # Integration (id=41), Release Pending (id=101) and Closed (id=61). These states are not
    # actually in numerical order, so we have to fudge some of them from their official
    # values, but NEW and DEVELOPMENT are the ones we really care about.
    # that are defined as exact statuses, the rest are pseudo-status that use other fields
    UNKNOWN = auto()
    NEW = 11
    PLANNING = 81
    DEVELOPMENT = 91
    INTEGRATION = 96
    READY_FOR_QA = auto()  # customfield_12321540 "Testing" (we set to Ready when readyForQA)
    TESTED = auto()  # customfield_12321540 "Testing" (set to Pass by QA, not us)
    RELEASE_PENDING = 101
    CLOSED = 161

    @classmethod
    def from_str(cls, status):
        """Return the JIStatus matching the status string, or JIStatus.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == str(status).replace(' ', '_').upper()), cls.UNKNOWN)


class BZResolution(IntEnum):
    """Possible resolution of a BZStatus.CLOSED BZ."""

    # https://bugzilla.redhat.com/page.cgi?id=fields.html#resolution
    UNKNOWN = auto()
    CURRENTRELEASE = auto()
    DUPLICATE = auto()
    ERRATA = auto()
    NOTABUG = auto()
    WONTFIX = auto()
    CANTFIX = auto()
    DEFERRED = auto()
    INSUFFICIENT_DATA = auto()
    NEXTRELEASE = auto()
    RAWHIDE = auto()
    UPSTREAM = auto()
    WORKSFORME = auto()
    EOL = auto()

    @classmethod
    def from_str(cls, resolution):
        """Return the BZResolution matching the status string, or BZResolution.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == resolution.upper()), cls.UNKNOWN)


class JIResolution(IntEnum):
    """Possible resolution of a JIStatus.CLOSED JIRA Issue."""

    UNKNOWN = auto()
    UNRESOLVED = auto()
    CURRENTRELEASE = auto()
    DUPLICATE = auto()
    ERRATA = auto()
    NOTABUG = auto()
    WONTFIX = auto()
    CANTFIX = auto()
    DEFERRED = auto()
    INSUFFICIENT_DATA = auto()
    NEXTRELEASE = auto()
    RAWHIDE = auto()
    UPSTREAM = auto()
    WORKSFORME = auto()
    EOL = auto()

    @classmethod
    def from_str(cls, resolution):
        """Return the JIResolution matching the status string, or JIResolution.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == resolution.upper()), cls.UNKNOWN)


class MrScope(IntEnum):
    """Possible scopes of an MR."""

    INVALID = auto()
    CLOSED = auto()
    NEEDS_REVIEW = auto()
    READY_FOR_QA = auto()
    READY_FOR_MERGE = auto()
    MERGED = auto()
    OK = auto()

    def label(self, prefix):
        """Return a formatted label string."""
        match self.name:
            case 'NEEDS_REVIEW':
                scope = NEEDS_REVIEW_SUFFIX
            case 'READY_FOR_QA':
                scope = NEEDS_TESTING_SUFFIX
            case 'READY_FOR_MERGE' | 'OK':
                scope = READY_SUFFIX
            case _:
                scope = self.name.capitalize()
        return f'{prefix}::{scope}'

    @classmethod
    def get(cls, type_str):
        """Return the MrScope that matches the type_str."""
        return cls.__members__.get(type_str.upper(), cls.INVALID)


class MrState(IntEnum):
    """Possible states of a GL MR."""

    # https://docs.gitlab.com/ee/api/graphql/reference/#mergerequeststate
    UNKNOWN = auto()
    ALL = auto()
    CLOSED = auto()
    LOCKED = auto()
    MERGED = auto()
    OPENED = auto()

    @classmethod
    def from_str(cls, state):
        """Return the MrState matching the state string, on MrState.UNKNOWN."""
        return next((member for name, member in cls.__members__.items()
                     if name == state.upper()), cls.UNKNOWN)


class DCOState(IntEnum):
    """Possible Commit DCO check results."""

    OK = auto()
    MISSING = auto()
    UNRECOGNIZED = auto()
    EMAIL_MATCHES = auto()
    NAME_MATCHES = auto()
    NOT_REDHAT = auto()

    @property
    def footnote(self):
        """Return the footnote string."""
        return DCO_FOOTNOTES[self]

    @property
    def title(self):
        """Return the name formatted pretty."""
        return self.name if self is DCOState.OK else self.name.replace('_', ' ').capitalize()


DCO_FOOTNOTES = {
    DCOState.OK: 'A matching valid DCO Signoff was found for this commit.',
    DCOState.MISSING: 'No valid DCO Signoff was found.',
    DCOState.UNRECOGNIZED: 'None of the commit Signoffs match the commit author name or email.',
    DCOState.EMAIL_MATCHES: 'A Signed-off-by email matches, but the names do not.',
    DCOState.NAME_MATCHES: 'A Signed-off-by name matches, but the email addresses do not.',
    DCOState.NOT_REDHAT: ('The commit author email address is not @redhat.com.'
                          '  Possibly the commit needs to have the author reset?')
}
