"""Tests for the base_mr mixins."""
from dataclasses import dataclass
from unittest import TestCase

import responses
from responses.matchers import json_params_matcher

from tests.test_approval_rules import ALL_MEMBERS_RULE
from tests.test_approval_rules import X86_RULE
from webhook import base_mr_mixins
from webhook.defs import GITFORGE
from webhook.graphql import GitlabGraph
from webhook.pipelines import PipelineResult
from webhook.users import UserCache

API_URL = f'{GITFORGE}/api/graphql'
NAMESPACE = 'group/project'
MR_ID = 123
MR_URL = f'{GITFORGE}/{NAMESPACE}/-/merge_requests/{MR_ID}'


class TestApprovalsMixin(TestCase):
    """Tests for the ApprovalsMixin."""

    @dataclass(repr=False)
    class BasicApprovals(base_mr_mixins.ApprovalsMixin, base_mr_mixins.GraphMixin):
        """A dummy class for testing the ApprovalsMixin."""
        user_cache: UserCache
        namespace: str = NAMESPACE
        iid: int = MR_ID

    @responses.activate
    def test_approvals_rules_property(self):
        """Returns a dict of the expected ApprovalRule objects."""
        graphql = GitlabGraph()
        user_cache = UserCache(graphql, NAMESPACE)
        match_query = base_mr_mixins.ApprovalsMixin.APPROVALS_QUERY.strip('\n')

        rules_list = [ALL_MEMBERS_RULE, X86_RULE]
        json_response = {'project': {'mr': {'approvalState': {'rules': rules_list}}}}
        responses.post(API_URL, json={'data': json_response},
                       match=[json_params_matcher({'query': match_query,
                                                   'variables': {'namespace': NAMESPACE,
                                                                 'mr_id': str(MR_ID)}},
                                                  strict_match=False)])

        basic_approvals = self.BasicApprovals(graphql, user_cache)
        found_rules = basic_approvals.approval_rules

        self.assertEqual(len(found_rules), 2)
        self.assertTrue('x86' in found_rules)
        self.assertTrue('All Members' in found_rules)


class TestPipelinesMixin(TestCase):
    """Tests for the PipelinessMixin."""

    @dataclass(repr=False)
    class BasicPipelines(base_mr_mixins.PipelinesMixin, base_mr_mixins.GraphMixin):
        """A dummy class for testing the PipelinesMixin."""
        user_cache: UserCache
        namespace: str = NAMESPACE
        iid: int = MR_ID

    @responses.activate
    def test_pipelines(self):
        """Returns a dict of the expected ApprovalRule objects."""
        graphql = GitlabGraph()
        user_cache = UserCache(graphql, NAMESPACE)
        match_query = base_mr_mixins.PipelinesMixin.PIPELINES_QUERY.strip('\n')

        ds_pipe = {'id': 'gid://Gitlab/pipeline/456', 'status': 'SUCCESS'}
        job_nodes = [{'id': 123, 'name': 'c9s_merge_request', 'downstreamPipeline': ds_pipe}]
        json_response = {'project': {'mr': {'headPipeline': {'jobs': {'nodes': job_nodes}}}}}
        rsp = responses.post(API_URL, json={'data': json_response},
                             match=[json_params_matcher({'query': match_query,
                                                         'variables': {'namespace': NAMESPACE,
                                                                       'mr_id': str(MR_ID)}},
                                                        strict_match=False)])

        basic_pipelines = self.BasicPipelines(graphql, user_cache)
        found_pipes1 = basic_pipelines.pipelines
        self.assertEqual(len(found_pipes1), 1)
        self.assertTrue(isinstance(found_pipes1[0], PipelineResult))
        self.assertEqual(rsp.call_count, 1)
        # Confirm the result is cached.
        found_pipes2 = basic_pipelines.pipelines
        self.assertEqual(rsp.call_count, 1)
        self.assertIs(found_pipes1, found_pipes2)
        # Confirm fresh_pipelines is the same but not the cached value.
        found_pipes3 = basic_pipelines.fresh_pipelines
        self.assertEqual(rsp.call_count, 2)
        self.assertIsNot(found_pipes3, found_pipes2)
        self.assertEqual(found_pipes3, found_pipes2)
        self.assertEqual(len(found_pipes3), 1)
        self.assertTrue(isinstance(found_pipes3[0], PipelineResult))
